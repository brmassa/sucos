using System.Globalization;
using System.Text.RegularExpressions;

namespace SuCoS.Helpers;

/// <summary>
/// Helper class to convert a string to a URL-friendly string.
/// </summary>
public static partial class UrlExtension
{
    [GeneratedRegex("[^a-zA-Z0-9]+")]
    private static partial Regex AlphaNumericRegex();

    [GeneratedRegex("[^a-zA-Z0-9.]+")]
    private static partial Regex AlphaNumericDotRegex();

    /// <summary>
    /// Converts a string to a URL-friendly string.
    /// It will remove all non-alphanumeric characters and replace spaces with the replacement character.
    /// </summary>
    /// <param name="title"></param>
    /// <param name="options"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static string ConvertToUrlFriendly(string? title, UrlSanitizationOptions? options = null)
    {
        title ??= string.Empty;
        options ??= new UrlSanitizationOptions(); // Use default options if not provided

        var cleanedTitle = !options.LowerCase ? title : title.ToLower(CultureInfo.CurrentCulture);

        var replacementChar = options.ReplacementChar ?? '\0';
        var replacementCharString = options.ReplacementChar.ToString() ?? string.Empty;

        // Remove non-alphanumeric characters and replace spaces with the replacement character
        cleanedTitle = (options.ReplaceDot ? AlphaNumericRegex() : AlphaNumericDotRegex())
            .Replace(cleanedTitle, replacementCharString)
            .Trim(replacementChar);

        return cleanedTitle;
    }

    /// <summary>
    /// Converts a path to a URL-friendly string.
    /// </summary>
    /// <param name="path"></param>
    /// <param name="options"></param>
    /// <returns></returns>
    public static string SanitizeUrlPath(string? path, UrlSanitizationOptions? options = null)
    {
        path ??= string.Empty;
        var items = path.Split("/");
        var result = (from item in items where !string.IsNullOrEmpty(item) select ConvertToUrlFriendly(item, options)).ToList();
        return (path.StartsWith('/') ? '/' : string.Empty) + string.Join('/', result);
    }

    /// <summary>
    /// Convert all paths to a unix path style
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string NormalizeToUnix(string? path) => (path ?? string.Empty).Replace('\\', '/');
}
