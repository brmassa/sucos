namespace SuCoS.Helpers;

/// <summary>
/// Options for the <see cref="UrlExtension"/> class.
/// Basically to force lowercase and to change the replacement character.
/// </summary>
public class UrlSanitizationOptions
{
    /// <summary>
    /// Force to generate lowercase URLs.
    /// </summary>
    public bool LowerCase { get; init; } = true;

    /// <summary>
    /// The character that will be used to replace spaces and other invalid characters.
    /// </summary>
    public char? ReplacementChar { get; init; } = '-';

    /// <summary>
    /// Replace dots with the replacement character.
    /// Note that it will break file paths and domain names.
    /// </summary>
    public bool ReplaceDot { get; init; }
}
