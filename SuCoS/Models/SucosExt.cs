namespace SuCoS.Models;

/// <summary>
/// (Code generated from SuCoSGenerator.cs) Build/compilation front matter.
/// </summary>
public static partial class SucosExt
{
    /// <summary>
    /// Date and time in UTC.
    /// </summary>
    public static DateTime BuildDate => _BuildDate();

    private static partial DateTime _BuildDate();

    /// <summary>
    /// Date and time (expressed as Ticks) in UTC.
    /// </summary>
    public static long BuildDateTicks() => _BuildDateTicks();

    private static partial long _BuildDateTicks();
}
