using Microsoft.Extensions.FileProviders;

namespace SuCoS.TemplateEngine;

/// <summary>
/// Same as PhysicalFileProvider, but corrects bug on Fluid that
/// add ".liquid" extension on every render/from call
/// </summary>
public class LiquidPhysicalFileProvider : PhysicalFileProvider, IFileProvider
{
    /// <summary>
    /// ctr
    /// </summary>
    /// <param name="root"></param>
    public LiquidPhysicalFileProvider(string root) : base(root) { }

    /// <summary>
    /// Remove the ".liquid" extenstion
    /// </summary>
    /// <param name="subpath"></param>
    /// <returns></returns>
    public new IFileInfo GetFileInfo(string subpath)
    {
        subpath = Path.ChangeExtension(subpath, null);
        return base.GetFileInfo(subpath);
    }
}
